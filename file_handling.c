//
// Created by Csurleny on 7/28/2022.
//

#include<stdio.h>
#include "file_handling.h"
#include "main.h"

extern UART_HandleTypeDef huart2;
#define UART &huart2

FATFS fs;  // file system
FIL fil; // File
FILINFO fno;
FRESULT fresult;  // result
UINT br, bw;  // File read/write count

// Card capacity
FATFS *pfs;
DWORD fre_clust;
uint32_t total, free_space;


int _write(int fd, char *ptr, int len) {
    HAL_UART_Transmit(UART, (uint8_t *) ptr, len, HAL_MAX_DELAY);
    return len;
}


void Mount_SD(const TCHAR *path) {
    fresult = f_mount(&fs, path, 1);
    if (fresult != FR_OK)
        printf("Error in mounting SD Card.\r\n");
    else
        printf("SD card mounted successfully.\r\n");
}

void Unmount_SD(const TCHAR *path) {
    fresult = f_mount(NULL, path, 1);
    if (fresult == FR_OK) 
        printf("SD card unmounted successfully.\r\n");
    else 
        printf("Error in unmounting SD card. \r\n");
}

FRESULT Format_SD(void) {
    DIR dir;
    char *path = malloc(20 * sizeof(char));
    sprintf(path, "%s", "/");

    fresult = f_opendir(&dir, path);
    if (fresult == FR_OK) {
        for (;;) {
            fresult = f_readdir(&dir, &fno);
            if (fresult != FR_OK || fno.fname[0] == 0) 
                break;
            if (fno.fattrib & AM_DIR){
                if (!(strcmp("SYSTEM~1", fno.fname))) 
                    continue;
                fresult = f_unlink(fno.fname);
                if (fresult == FR_DENIED) 
                    continue;
            } else {
                fresult = f_unlink(fno.fname);
            }
        }
        f_closedir(&dir);
    }
    free(path);
    return fresult;
}


FRESULT Write_File(char *name, char *data) {
    fresult = f_stat(name, &fno);
    if (fresult != FR_OK) {
        printf("Error, %s does not exists.\r\n", name);
        return fresult;
    } else {
        fresult = f_open(&fil, name, FA_OPEN_EXISTING | FA_WRITE);
        if (fresult != FR_OK) {
            printf("Error No. %d in opening file %s.\r\n", fresult, name);
            return fresult;
        } else {
            fresult = f_write(&fil, data, strlen(data), &bw);
            if (fresult != FR_OK) {
                printf("Error No. %d while writing to the file %s.\r\n", fresult, name);
            }
            fresult = f_close(&fil);
            if (fresult != FR_OK) {
                printf("Error No. %d in closing file %s after writing it.\r\n", fresult, name);
            } else {
                printf("File %s is written and it is closed successfully.\r\n", name);
            }
        }
        return fresult;
    }
}

FRESULT Read_File(char *name) {
    fresult = f_stat(name, &fno);
    if (fresult != FR_OK) {
        printf("Error, %s does not exists\r\n", name);
        return fresult;
    } else {
        fresult = f_open(&fil, name, FA_READ);
        if (fresult != FR_OK) {
            printf("Error No. %d in opening file %s\r\n", fresult, name);
            return fresult;
        }
        char *buffer = malloc(sizeof(f_size(&fil)));
        fresult = f_read(&fil, buffer, f_size(&fil), &br);
        if (fresult != FR_OK) {
            char *buf = malloc(100 * sizeof(char));
            free(buffer);
            printf("Error No. %d in reading file %s\r\n", fresult, name);
        } else {
            printf("%s", buffer);
            free(buffer);

            fresult = f_close(&fil);
            if (fresult != FR_OK) {
                char *buf = malloc(100 * sizeof(char));
                printf("Error No. %d in closing file %s\r\n", fresult, name);
            } else {
                printf("File %s was closed successfully\r\n", name);
            }
        }
        return fresult;
    }
}

FRESULT Create_File(char *name) {
    fresult = f_stat(name, &fno);
    if (fresult == FR_OK) {
        printf("Error %s already exists.\t Use Update_File \r\n", name);
        return fresult;
    } else {
        fresult = f_open(&fil, name, FA_CREATE_ALWAYS | FA_READ | FA_WRITE);
        if (fresult != FR_OK) {
            printf("Error No. %d in creating file %s\r\n", fresult, name);
            return fresult;
        } else {
            printf("%s created successfully.\t Now use Write_File to write data.\r\n", name);
        }

        fresult = f_close(&fil);
        if (fresult != FR_OK) {
            printf("Error No. %d in closing file %s\r\n", fresult, name);
        } else {
            printf("File *%s* was closed successfully.\r\n", name);
        }
    }
    return fresult;
}

FRESULT Update_File(char *name, char *data) {
    fresult = f_stat(name, &fno);
    if (fresult != FR_OK) {
        printf("Error, %s does not exists.\r\n", name);
        return fresult;
    } else {
        fresult = f_open(&fil, name, FA_OPEN_APPEND | FA_WRITE);
        if (fresult != FR_OK) {
            printf("Error No. %d in opening file %s.\r\n", fresult, name);
            return fresult;
        }
        fresult = f_write(&fil, data, strlen(data), &bw);
        if (fresult != FR_OK) {
            printf("Error No. %d in writing file %s.\r\n", fresult, name);
        } else {
            printf("%s was updated successfully.\r\n", name);
        }
        fresult = f_close(&fil);
        if (fresult != FR_OK) {
            printf("Error No. %d in closing file %s.\r\n", fresult, name);
        } else {
            printf("File %s was closed successfully.\r\n", name);
        }
    }
    return fresult;
}

FRESULT Remove_File(char *name) {
    fresult = f_stat(name, &fno);
    if (fresult != FR_OK) {
        printf("Error, %s does not exists\t\n", name);
        return fresult;
    } else {
        fresult = f_unlink(name);
        if (fresult == FR_OK) {
            printf("%s has been removed successfully.\r\n", name);
        } else {
            printf("Error No. %d in removing %s.\r\n", fresult, name);
        }
    }
    return fresult;
}

FRESULT Create_Dir(char *name) {
    fresult = f_mkdir(name);
    if (fresult == FR_OK) {
        printf("%s has been created successfully.\r\n", name);
    } else {
        printf("Error No. %d in creating directory %s.\r\n", fresult, name);
    }
    return fresult;
}

void Check_SD_Space(void) {
    f_getfree("", &fre_clust, &pfs);
    total = (uint32_t) ((pfs->n_fatent - 2) * pfs->csize * 0.5);
    printf("SD card total Size:\t%lu\r\n", total);
    free_space = (uint32_t) (fre_clust * pfs->csize * 0.5);
    printf("SD card free space:\t%lu\r\n", free_space);
}

