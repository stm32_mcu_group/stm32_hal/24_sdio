//
// Created by Csurleny on 7/28/2022.
//

#ifndef INC_24_SDIO_FILE_HANDLING_H
#define INC_24_SDIO_FILE_HANDLING_H

#include "fatfs.h"
#include "string.h"
#include "stdio.h"
#include "fatfs.h"

void Mount_SD (const TCHAR* path);
void Unmount_SD (const TCHAR* path);
FRESULT Format_SD (void);

FRESULT Write_File (char *name, char *data);
FRESULT Read_File (char *name);
FRESULT Create_File (char *name);
FRESULT Remove_File (char *name);
FRESULT Create_Dir (char *name);
void Check_SD_Space (void);
FRESULT Update_File (char *name, char *data);

#endif //INC_24_SDIO_FILE_HANDLING_H
